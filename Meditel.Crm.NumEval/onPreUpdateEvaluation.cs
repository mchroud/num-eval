﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System.Globalization;


namespace Meditel.CRM.Plugin.NumEval
{
    /// <summary>
    /// Mise jour de la numerotation et date derniére evaluation  lors du changement de l'utilisateur au niveau de la fiche d'evaluation 
    /// Mise a jour du titre de l'evaluation avec le nouvel utilisateur 
    /// </summary>
    public class onPreUpdateEvaluation : IPlugin
    { 
        public void Execute(IServiceProvider serviceProvider)
        {
            try
            {
                Entity evaluation = new Entity();
                Entity PreImg = new Entity();
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    evaluation = (Entity)context.InputParameters["Target"];
                    PreImg = (Entity)context.PreEntityImages["PreImg"];

                    if (evaluation.LogicalName != "med_fichedvaluation") { return; }
                }
                else
                {
                    return;
                }
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
                if (evaluation.Contains("med_utilisateur") && PreImg.Contains("med_utilisateur"))
                {
                 
                    EntityReference _user = (EntityReference)evaluation.Attributes["med_utilisateur"];
                    EntityReference _userPreImg = (EntityReference)PreImg.Attributes["med_utilisateur"];


            if (_user!=null && _userPreImg!=null){

                #region Mise a jour Numerotation et date dernier eval de l'ancien utilisateur 
                DateTime dateDerEvalOld;
                string fetchOldUserLastEval = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
  <entity name='med_fichedvaluation'>
    <attribute name='createdon' />
    <order attribute='createdon' descending='true' />
    <filter type='and'>
      <condition attribute='statecode' operator='eq' value='0' />
	  <condition attribute='med_utilisateur' operator='eq' uiname='' uitype='systemuser' value='" + _userPreImg.Id + @"' />
    </filter>
  </entity>
</fetch>";

                EntityCollection ResfetchOldUserLastEval = service.RetrieveMultiple(new FetchExpression(fetchOldUserLastEval));
                if (ResfetchOldUserLastEval.Entities.Count > 0)
                {
                    dateDerEvalOld = ResfetchOldUserLastEval[0].GetAttributeValue<DateTime>("createdon");
                    Entity oldUser = new Entity("systemuser");
                    oldUser.Id = _userPreImg.Id;
                    oldUser["med_numeroderniereeval"] = ResfetchOldUserLastEval.Entities.Count;
                    oldUser["med_datederniereevaluation"] = dateDerEvalOld;
                    service.Update(oldUser);

                }

                #endregion
                #region Mise a jour Titre fiche avec le nouvel utilisateur et Numerotation et date dernier eval de le nouvel utilisateur
                string fetchUser = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
  <entity name='systemuser'>
    <attribute name='fullname' />
    <attribute name='systemuserid' />
    <attribute name='med_numeroderniereeval' />
    <attribute name='med_datederniereevaluation' />
    <order attribute='fullname' descending='false' />
    <filter type='and'>
      <condition attribute='systemuserid' operator='eq' uiname='' uitype='systemuser' value='" + _user.Id + "' />" +
                @"</filter>
  </entity>
</fetch>";
                EntityCollection ResFetchUser = service.RetrieveMultiple(new FetchExpression(fetchUser));
                if (ResFetchUser.Entities.Count > 0)
                {
                    Entity sysuser = new Entity();
                    sysuser.LogicalName = _user.LogicalName;
                    sysuser.Id = _user.Id;
                    int derNum = 1;
                    int nextNum;
                    DateTime dateDerEval = DateTime.Now;
                    if (!ResFetchUser[0].Contains("med_numeroderniereeval") || !ResFetchUser[0].Contains("med_datederniereevaluation"))
                    {
                        sysuser["med_numeroderniereeval"] = derNum;
                        sysuser["med_datederniereevaluation"] = dateDerEval;
                        evaluation["med_numeval"] = derNum;
                        nextNum = derNum;
                        service.Update(sysuser);
                        if (evaluation.Contains("med_semaines") && ResFetchUser[0].Contains("fullname"))
                        {
                            evaluation["med_name"] = ResFetchUser[0].Attributes["fullname"] + " Semaine " + evaluation.Attributes["med_semaines"] + " EVAL N° " + nextNum;
                        }
                        return;

                    }
                    if (ResFetchUser[0].Contains("med_numeroderniereeval") && ResFetchUser[0].Contains("med_datederniereevaluation"))
                    {
                        dateDerEval = ResFetchUser[0].GetAttributeValue<DateTime>("med_datederniereevaluation");
                        derNum = ResFetchUser[0].GetAttributeValue<int>("med_numeroderniereeval");
                        if ((dateDerEval.Year == DateTime.Now.Year) && (dateDerEval.Month == DateTime.Now.Month))
                        {
                            sysuser["med_numeroderniereeval"] = derNum + 1;
                            sysuser["med_datederniereevaluation"] = DateTime.Now;
                            evaluation["med_numeval"] = derNum + 1;
                            nextNum = derNum + 1;
                            service.Update(sysuser);
                            if (evaluation.Contains("med_semaines") && ResFetchUser[0].Contains("fullname"))
                            {
                                
                                evaluation["med_name"] = ResFetchUser[0].Attributes["fullname"] + " Semaine " + evaluation.Attributes["med_semaines"] + " EVAL N° " + nextNum;
                            }
                            return;
                        }
                        else
                        {
                            sysuser["med_numeroderniereeval"] = 1;
                            sysuser["med_datederniereevaluation"] = DateTime.Now;
                            evaluation["med_numeval"] = derNum;
                            nextNum = derNum;
                            service.Update(sysuser);
                            if (evaluation.Contains("med_semaines") && ResFetchUser[0].Contains("fullname"))
                            {
                                evaluation["med_name"] = ResFetchUser[0].Attributes["fullname"] + " Semaine " + evaluation.Attributes["med_semaines"] + " EVAL N° " + nextNum;
                            }
                            return;
                        }
                    }
                    else
                    {
                        throw new InvalidPluginExecutionException("onPreUpdateEvaluation,Execute. error on plug-in onPreUpdateEvaluation:Numérotation ");

                    }

                } 
                #endregion
                }
                }

            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("onPreUpdateEvaluation,Execute. error on plug-in onPreUpdateEvaluation:Numérotation ");

            }
        }
    }
}
